import csv
import re
from typing import Pattern


with open("regex/phonebook_raw.csv") as f:
    rows = csv.reader(f, delimiter=",")
    contacts_list = list(rows)
 
persons = {}
for contact in contacts_list:

    parts_name  = ' '.join(contact).split(' ')[:3]
    lastname = parts_name[0]
    firstname = parts_name[1]
    surname = parts_name[2]
    

    pattern=r'(\+7|8)?\s*\(*(\d{3})\)*\s*\-*(\d{3})\s*\-*(\d{2})\s*\-*(\d{2})\s?\(?(\д?\о?\б?\.)?\s?(\d{4})?\)?'
    phone  = re.sub(pattern, r'+7(\2)\3-\4-\5 \6\7', contact[5])

    contact[0] = lastname
    contact[1] = firstname
    contact[2] = surname
    contact[5] = phone
    
    if not f'{lastname.lower()} {firstname.lower()}' in persons.keys():
        persons[f'{lastname.lower()} {firstname.lower()}'] = contact
    else:
        exists_contact = persons[f'{lastname.lower()} {firstname.lower()}']
        for i in range(len(exists_contact)):
            if not exists_contact[i]:
               exists_contact[i] = contact[i]



with open("phonebook.csv", "w") as f:
  datawriter = csv.writer(f, delimiter=',')
  datawriter.writerows(persons.values())